provider "aws" {
  version = "~> 2.0"
  region  = "us-west-2"
}

resource "aws_security_group" "example" {
  name        = "example"
  description = "Allow inbound traffic for ssh"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "example"
  }
}

resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "aws_key_pair" "example" {
  key_name   = "example"
  public_key = "${tls_private_key.example.public_key_openssh}"
}

resource "aws_instance" "web" {
  ami             = "ami-0cb72367e98845d43"
  instance_type   = "t2.micro"
  key_name        = "${aws_key_pair.example.key_name}"
  security_groups = ["${aws_security_group.example.name}"]

  tags = {
    Name        = "HelloWorld"
    Role        = "WebServer"
    Environment = "Test"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = "${tls_private_key.example.private_key_pem}"
      host        = "${aws_instance.web.public_ip}"
    }
    inline = [
      "whoami"
    ]
  }
}
